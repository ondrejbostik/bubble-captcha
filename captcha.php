<?php
require_once('letters.php');
require_once('captcha_class.php');

$diff_lvl = 1;
if(isset($_GET["diff_lvl"]))
  if (($_GET["diff_lvl"] >0) && ($_GET["diff_lvl"] < 4))
    $diff_lvl = $_GET["diff_lvl"];  

if ($diff_lvl == 1)
  $captcha = new Captcha($letters, array(5, 7), 10 );
else if ($diff_lvl == 2)
  $captcha = new Captcha($letters, array(5, 7), 10, 10, 20 );
else //if ($diff_lvl == 3) 
  $captcha = new Captcha($letters, array(5, 7), 10, 15, 30 );

$captcha->getImage();

/*
echo "<pre>";
print_r($captcha);
echo "</pre>";
/**/