<?php

class Point
{
  private static $raster = 35;
  private static $diameter = 30;
  private static $movement = 0;
  private static $diam_var = 0;
  
  public $row;
  public $col;
  public $cat;
  
  public $x;
  public $y;
  public $d;
  
  public function Point($r, $c, $cat)
  {
    $this->row = $r;
    $this->col = $c;
    $this->cat = $cat;
    
    $this->x = round(($this->row+(rand(100-self::$movement, 100+self::$movement)/100)) * self::$raster);
    $this->y = round(($this->col+(rand(100-self::$movement, 100+self::$movement)/100)) * self::$raster);
    $this->d = self::$diameter * (rand(100-self::$diam_var, 100+self::$diam_var)/100);
  }
  
  public function getRaster() { return self::$raster; }
  
  public static function setMovement($mv) { self::$movement = $mv; }
  public static function setDiameterVar($dv) { self::$diam_var = $dv; }
}

class Captcha
{
  
  public $rows = 0;
  public $cols = 0;
  public $points = array();
  public $message = '';
  public $msgLen = 0;
  private $letters;
  
  public function Captcha($letters, $generate=false, $r=5, $movement=0, $diameterVar=0)
  {
    $this->letters = $letters;
    $this->rows = $r;
    $this->cols = 1;
    
    Point::setMovement($movement);
    Point::setDiameterVar($diameterVar);
    
    for($i=0; $i<$r; $i++)
      $this->points[] = new Point(0, $i, '0');
    
    $this->msgLen = 0;
    if ($generate != false)
    {
      if (is_array($generate)) {
        if (count($generate) == 2)
        {
          $this->msgLen = rand($generate[0], $generate[1]);
        } else
        {
          $this->msgLen = $generate[0];
        }
      } else
      {
        $this->msgLen = $generate;
      }
    }
    
    if ( $this->msgLen>0 )
      $this->msgGen();
  }
  
  private function msgGen()
  {
    $alphabet = "ABCDEFGHIJKLMNPQRSTUVXYZ123456789";
    for($i=0; $i<$this->msgLen; $i++)
    {
      $this->message[$i] = $alphabet[rand(0, strlen($alphabet)-1)];
      $this->addLetter($this->message[$i]);
    }
  }
    
  public function addLetter($l)
  {
    $l_rows = sizeof($this->letters[$l]);
    $l_cols = sizeof($this->letters[$l][0]);
    
    $k=1;
    
    //for previous letters
    if ($this->rows < $l_rows+2)
    {
      //complete bottom white space in prew letters
      for($i=$this->rows; $i<$l_rows+2; $i++)
        for($j=0; $j<$this->cols; $j++)
          $this->points[] = new Point($j, $i, '0');
      $this->rows = $l_rows+2;
    } else {
      $k=rand(1, $this->rows-$l_rows-1);
    } 
    
    for($j=0; $j<$l_cols; $j++)
    {
      //Add upper line
      for($i=0; $i<$k; $i++)
        $this->points[] = new Point($j+$this->cols, $i, '0');
    
      //Add letter
      for($i=0; $i<$l_rows; $i++)
        $this->points[] = new Point($j+$this->cols, $i+$k, $this->letters[$l][$i][$j]);
        
      //Add lower line
      for($i=$l_rows+$k; $i<$this->rows; $i++)
        $this->points[] = new Point($j+$this->cols, $i, '0');    
    }
    
    $this->cols += $l_cols;
    
    //Add space
    for($i=0; $i<$this->rows; $i++)
      $this->points[] = new Point($this->cols, $i, '0');
    $this->cols++;     
  }
  
  public function getImage($label=false)
  {
    session_start();
    header('Content-type: image/png');
    header("Cache-Control: no-cache, must-revalidate"); 
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); /**/
    
    $png_image = imagecreatetruecolor($this->points[0]->getRaster()*($this->cols+1), $this->points[0]->getRaster()*($this->rows+1));
    
    $white = imagecolorallocate($png_image, 255, 255, 255);
    $grey = imagecolorallocate($png_image, 229, 229, 229);
    $green = imagecolorallocate($png_image, 128, 204, 204);
    $red = imagecolorallocate($png_image, 255, 0, 0);
    imagefilltoborder($png_image, 0, 0, $white, $white);
    
    foreach($this->points as $point)
    {
      $color = $green;
      if ($point->cat == 1)
        $color = $red;

      imagefilledellipse($png_image, $point->x, $point->y, $point->d, $point->d, $color);
    }
    
    if ($label) {
      $font_file = './arial.ttf';
      imagefttext($png_image, $this->points[0]->getRaster(), 0, 0, $this->points[0]->getRaster(), $red, $font_file, implode($this->message));
    }
    $_SESSION['captcha'] = implode($this->message);
    
    imagepng($png_image);
    imagedestroy($png_image);
  }
}