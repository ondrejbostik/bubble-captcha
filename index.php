<?php
  session_start();
  
  if(isset($_POST["security"]) and $_POST["security"] != '')
  {
    
    if(strtolower($_POST["security"]) == strtolower($_SESSION['captcha']))
      $success=1;
    else
      $success=0;
      
    require_once('sql_config.php');  
      
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    $name = $_POST['name'];
    if ($name == '')
      $name = "noname";
      
    $diff = $_POST['diff_lvl'];
    if ($diff == '')
      $diff = 1;
    
    $sql = "INSERT INTO captcha (diff_lvl, jmeno, status, str_gen, str_entered) VALUES
            ('" . $diff . "','" . $name . "', '" . $success . "', '" . strtolower($_SESSION['captcha']) . "', '" . strtolower($_POST["security"]) . "')";

    if ($conn->query($sql) === TRUE) {
      $_SESSION['captcha'] = "";
      $_POST["security"] = "";
      $conn->close();
      
      header("Location: http://".$_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"] . "?success=" . $success ."&name=".$_POST['name'] ."&diff_lvl=".$diff );
      exit;
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
  
  $link = "http://".$_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"] . "?";
  if(isset($_GET['name'])) $link .= "name=" . $_GET['name'] . "&";
  $link .= "diff_lvl=";
  
  $diff_lvl = isset($_GET["diff_lvl"]) ? $_GET["diff_lvl"] : 1;
  
?>
<html lang='cs'>
  <head>
    <title>Captcha testing</title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <style type="text/css">
      .container {width: 800px; padding: 20px; margin: 10px auto; background: grey;}
      h1, h2 { margin: 20px 0;}
      h4 { margin: 5px 0 10px;}
      table {width: 100%}
      .btn, input[type=submit] {
        background: #3498db;
        background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
        background-image: -moz-linear-gradient(top, #3498db, #2980b9);
        background-image: -ms-linear-gradient(top, #3498db, #2980b9);
        background-image: -o-linear-gradient(top, #3498db, #2980b9);
        background-image: linear-gradient(to bottom, #3498db, #2980b9);
        -webkit-border-radius: 28;
        -moz-border-radius: 28;
        border-radius: 28px;
        font-family: Arial;
        color: #ffffff;
        font-size: 20px;
        padding: 10px 20px 10px 20px;
        text-decoration: none;
        margin: 20px;
      }
      
      .btn:hover, input[type=submit] {
        background: #3cb0fd;
        background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
        background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
        background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
        background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
        background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
        text-decoration: none;
      }
      
      .btn-selected {
        background: #3df24f;
        background-image: -webkit-linear-gradient(top, #3df24f, #2bb834);
        background-image: -moz-linear-gradient(top, #3df24f, #2bb834);
        background-image: -ms-linear-gradient(top, #3df24f, #2bb834);
        background-image: -o-linear-gradient(top, #3df24f, #2bb834);
        background-image: linear-gradient(to bottom, #3df24f, #2bb834);
      }
      
      .btn-selected:hover {
        background: #22c714;
        background-image: -webkit-linear-gradient(top, #22c714, #16750b);
        background-image: -moz-linear-gradient(top, #22c714, #16750b);
        background-image: -ms-linear-gradient(top, #22c714, #16750b);
        background-image: -o-linear-gradient(top, #22c714, #16750b);
        background-image: linear-gradient(to bottom, #22c714, #16750b);
      }
      
      .btn-wrap {
        padding: 20px;
      }
      .del2 {
        width: 50%;
        float: left;
        text-align: center;
        margin: 10px 0;
      }
      .del3 {
        width: 33%;
        float: left;
        text-align: center;
        margin: 0;
      }
      .center {
        text-align: center;
        margin: 10px;
      }
      .clearfix {     clear: both; }
      td {
          text-align: center;
      }
      
      .total-stats {
        width: 65%;
        margin: 0 auto;  
      }
      .total-stats th{
        width: 50%;
        padding: 5px 10px;  
      }
      .total-stats td{
        width: 50%;
        padding: 5px 10px;
        text-align: left;  
      }
    </style>
  </head>
  

  
  <body>     
  <div class="container" style="background: <?php if(isset($_GET["success"])) echo ($_GET["success"] == "1") ? "green" : "red"; ?>">
  
  <div style="text-align: center;">
    <h1>Captcha code testing platform</h1>
    <h1>Testovací platforma CAPTCHA kódů</h1>
  

    <div class="btn-wrap">
      <a href="<?php echo $link; ?>1" class="btn<?php if($diff_lvl == 1) echo " btn-selected"; ?>">
          Difficulty 1
      </a>
      <a href="<?php echo $link; ?>2" class="btn<?php if($diff_lvl == 2) echo " btn-selected"; ?>">
          Difficulty 2
      </a>
      <a href="<?php echo $link; ?>3" class="btn<?php if($diff_lvl == 3) echo " btn-selected"; ?>">
          Difficulty 3
      </a>
    </div>  
  </div>

     
    <form method="post">
        <input type="hidden" name="diff_lvl" value="<?php if(isset($_GET["diff_lvl"])) echo $_GET["diff_lvl"]; ?>">
        <div style="margin: 0 auto; width: 600px">
          <img src="captcha.php?diff_lvl=<?php echo $diff_lvl; ?>" width="600px"/>
        </div>
        <div class="del2">
            <label for="name">Name or Nickname:</label> <br />
            <input type="text" id="name" name="name" value="<?php if(isset($_GET["name"])) echo $_GET["name"]; ?>">
        </div>
        <div class="del2">
            <label for="security">Rewrite text from picture:</label> <br />
            <input type="text" id="security" name="security" autofocus>
        </div>
        <div class="clearfix"></div>
        <div class="center">        
          <input type="submit" value="Send">
        </div>
    </form>
  </div>
  
  <div class="container">  
  <h2>En - About</h2>
  
  <p>This form is part of our research of a new design of CAPTCHA schemes (an acronym for
  "Completely Automated Public Turing test to tell Computers and Humans Apart"). Participation in research
  helps us to better secure web services from attackers. Furthermore, this research can help improve current OCR algorithms.</p>
  
  <p><b>Instructions:</b> For every person, who would like to take part in research. Please select unique name,
  which is not listed in the table below and write it to the field on the left. Then try to rewrite text from image
  to the field on the right. You will send your answer to us by clicking the Send button or pressing Enter on your keyboard.
  The green color of this form means, that your answer is correct. Red color means an unsuccessful attempt. You can switch between
  different levels of difficulty with the three buttons on top of the page.</p>
  
  <p><b>Code is case INSENSITIVE,</b> you can use lowercase or uppercase letters.</p>
  <p>Please transcribe at least <b>5-10 different pictures</b> for <b>each difficulty</b> level.</p>
  
  <p><b>Thank you!</b></p>
  
  <h2>CZ - O výzkumu</h2>
  
  <p>Tato strának je součástí našeho výzkumu nových metod použitých pro úlohu CAPTCHA.
  Účastí na tomto výzkumu se budete podílet na vylepšení zabezpečení webových stránek před útočníky.
  Výzkum navíc přispívá i do dalších oblastí, jako je například vylepšení stávajících OCR algoritmů.</p>
  
  <p><b>Instrukce:</b> Pokud se chcete zúčastnit výzkumu, prosím, vyberte si unikátní jméno, které není již použito v tabulce
  a vepište ho do levého textového pole. Poté se pokuste přepsat kód z obrázku do pravého textového pole. Vaši odpověď odešlete
  kliknutím na tlačítko Send, nebo stisknutím klávesy Enter. Následně se formulář přebarví podle vaší odpovědi. Zelená barva značí
  úspěch, červená neúspěch. Mezi jednotlivými úrovni obtížnosti se můžete přepínat třemi tlačítky Difficulty nad obrázkem.</p>
  
  <p><b>Kódy nezávisí na velikosti písmen,</b> můžete používat malá nebo velká písmena podle uvážení.</p>
  <p>Prosím, pokuste se vyplnit <b>5-10 různých obrázků</b> pro každou <b>úroveň obtížnosti.</b></p>
  
  <p><b>Děkujeme Vám!</b></p>
      
    </div>
<?php require_once('stats.php'); ?>
  </body>
</html>