<?php

define('COLS', 3);
define('ROWS', 5);

$letters = array();

$letters['A'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(1,1,1),
  array(1,0,1),
  array(1,0,1)
);

$letters['.'] = array
(
  array(1)
);

$letters['|'] = array
(
  array(1),
  array(1),
  array(1),
  array(1),
  array(1),
  array(1),
  array(1)
);

$letters['B'] = array
(
  array(1,1,0),
  array(1,0,1),
  array(1,1,0),
  array(1,0,1),
  array(1,1,0)
);

$letters['C'] = array
(
  array(0,1,1),
  array(1,0,0),
  array(1,0,0),
  array(1,0,0),
  array(0,1,1)
);

$letters['D'] = array
(
  array(1,1,0),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(1,1,0)
);

$letters['E'] = array
(
  array(1,1,1),
  array(1,0,0),
  array(1,1,0),
  array(1,0,0),
  array(1,1,1)
);

$letters['F'] = array
(
  array(1,1,1),
  array(1,0,0),
  array(1,1,0),
  array(1,0,0),
  array(1,0,0)
);

$letters['G'] = array
(
  array(0,1,1),
  array(1,0,0),
  array(1,0,1),
  array(1,0,1),
  array(0,1,1)
);

$letters['H'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(1,1,1),
  array(1,0,1),
  array(1,0,1)
);

$letters['I'] = array
(
  array(1,1,1),
  array(0,1,0),
  array(0,1,0),
  array(0,1,0),
  array(1,1,1)
);

$letters['J'] = array
(
  array(0,1,1),
  array(0,0,1),
  array(0,0,1),
  array(1,0,1),
  array(0,1,0)
);

$letters['K'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(1,1,0),
  array(1,0,1),
  array(1,0,1)
);

$letters['L'] = array
(
  array(1,0,0),
  array(1,0,0),
  array(1,0,0),
  array(1,0,0),
  array(1,1,1)
);

$letters['M'] = array   //TODO
(
  array(1,1,0,1,0),
  array(1,0,1,0,1),
  array(1,0,1,0,1),
  array(1,0,1,0,1),
  array(1,0,1,0,1)
);

$letters['N'] = array
(
  array(1,1,0),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1)
);

$letters['O'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(0,1,0)
);

$letters['P'] = array
(
  array(1,1,0),
  array(1,0,1),
  array(1,1,0),
  array(1,0,0),
  array(1,0,0)
);

$letters['Q'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(0,1,1)
);

$letters['R'] = array
(
  array(1,1,0),
  array(1,0,1),
  array(1,1,0),
  array(1,0,1),
  array(1,0,1)
);

$letters['S'] = array
(
  array(0,1,1),
  array(1,0,0),
  array(1,1,1),
  array(0,0,1),
  array(1,1,0)
);

$letters['T'] = array
(
  array(1,1,1),
  array(0,1,0),
  array(0,1,0),
  array(0,1,0),
  array(0,1,0)
);

$letters['U'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(1,1,1)
);

$letters['V'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(0,1,0)
);

$letters['W'] = array
(
  array(1,0,1,0,1),
  array(1,0,1,0,1),
  array(1,0,1,0,1),
  array(1,0,1,0,1),
  array(0,1,0,1,1)
);

$letters['X'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(0,1,0),
  array(1,0,1),
  array(1,0,1)
);

$letters['Y'] = array
(
  array(1,0,1),
  array(1,0,1),
  array(0,1,0),
  array(0,1,0),
  array(0,1,0)
);

$letters['Z'] = array
(
  array(1,1,1),
  array(0,0,1),
  array(0,1,0),
  array(1,0,0),
  array(1,1,1)
);




$letters['0'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(1,0,1),
  array(1,0,1),
  array(0,1,0)
);

$letters['1'] = array
(
  array(0,0,1),
  array(0,1,1),
  array(1,0,1),
  array(0,0,1),
  array(0,0,1)
);

$letters['2'] = array
(
  array(1,1,0),
  array(0,0,1),
  array(0,1,0),
  array(1,0,0),
  array(1,1,1)
);

$letters['3'] = array
(
  array(1,1,0),
  array(0,0,1),
  array(1,1,0),
  array(0,0,1),
  array(1,1,0)
);

$letters['4'] = array
(
  array(0,1,0),
  array(1,0,0),
  array(1,1,1),
  array(0,1,0),
  array(0,1,0)
);

$letters['5'] = array
(
  array(1,1,1),
  array(1,0,0),
  array(0,1,1),
  array(1,0,1),
  array(0,1,0)
);

$letters['6'] = array
(
  array(0,1,1),
  array(1,0,0),
  array(0,1,0),
  array(1,0,1),
  array(0,1,0)
);

$letters['7'] = array
(
  array(1,1,1),
  array(0,0,1),
  array(0,1,0),
  array(0,1,0),
  array(0,1,0)
);

$letters['8'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(0,1,0),
  array(1,0,1),
  array(0,1,0)
);

$letters['9'] = array
(
  array(0,1,0),
  array(1,0,1),
  array(0,1,1),
  array(0,0,1),
  array(0,0,1)
);