<?php  //<pre>
header('Content-Disposition: attachment; filename="statistics.tex";');

require_once('sql_config.php');

$variant = array("a", "b", "c");

$alphabet = str_split("ABCDEFGHIJKLMNPQRSTUVXYZ123456789");

?>%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%         Stats 1         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

<?php $conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT count(*) all_count, (SELECT COUNT(*) FROM captcha WHERE status = 1) sok, COUNT(DISTINCT jmeno) users FROM captcha";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) :
  if($row = mysqli_fetch_assoc($result)) : ?>
    \def \uniqueUsers {<?php echo $row["users"]; ?>}
    \def \totalAttemptsCount {<?php echo $row["all_count"]; ?>}
    \def \totalSuccessAttempts {<?php echo $row["sok"]; ?>}
    \def \totalFailedAttempts {<?php echo $row["all_count"] - $row["sok"]; ?>}
    \def \totalSuccessRate {<?php echo round(($row["sok"] / $row["all_count"])*100); ?>}
  <?php endif; ?>
<?php else : ?>
  \def \totalAttemptsCount {572}
  \def \totalSuccessAttempts {389}
  \def \totalFailedAttempts {183}
  \def \totalSuccessRate {68}
<?php endif; ?>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%         Stats 2         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def \totalStatisticsPerDifficultyLevel {

  \begin{table}[h] 
  	\begin{center}
  		\caption{Total statistics per difficulty level} 
  		\label{tab:totalStats}
  		\begin{tabular}{ccccc}
  			\hline\noalign{\smallskip}
  			Level of difficulty &  Total attempts count & Succes attempts count & Failed attempts count & Success rate \\
  			\noalign{\smallskip}
  			\hline\noalign{\smallskip}
  <?php
  $sql = "SELECT diff_lvl, count(*) all_count, (SELECT COUNT(*) FROM captcha a WHERE a.diff_lvl = b.diff_lvl AND a.status = 1) sok FROM captcha b GROUP BY diff_lvl";
  $result = mysqli_query($conn, $sql);
  if (mysqli_num_rows($result) > 0) :
    while($row = mysqli_fetch_assoc($result)) : ?>
  			Variant <?php echo $variant[$row["diff_lvl"]-1]; ?> & <?php echo $row["all_count"]; ?> & <?php echo $row["sok"]; ?> & <?php echo $row["all_count"] - $row["sok"]; ?> & <?php echo round(($row["sok"] / $row["all_count"])*100); ?>\%  \\
    <?php endwhile; ?>
  <?php else : ?>
  			1 & 153 & 110 & 43 & 72\%  \\
  			2 & 285 & 191 & 94 & 67\%  \\
  			3 & 134 &  88 & 46 & 66\%  \\
  <?php endif; ?>
  			Sum & \totalAttemptsCount & \totalSuccessAttempts & \totalFailedAttempts & \totalSuccessRate\% \\
  			\hline
  		\end{tabular}
  	\end{center}
  \end{table}	

}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%         Stats 3         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%                         %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

<?php $sql = "SELECT str_gen, str_entered, diff_lvl FROM `captcha`";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  
  $stats = array();
  foreach($alphabet as $char)
  {
    $stats_1[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
    $stats_2[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
    $stats_3[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
  }
  while($row = mysqli_fetch_assoc($result))
  {
    if (strlen($row["str_gen"]) == strlen($row["str_entered"]))
      for ($i=0; $i<strlen($row["str_gen"]); $i++) {
        $dl = $row["diff_lvl"];
        if($row["str_gen"][$i] == $row["str_entered"][$i])
          ${"stats_$dl"}[strtoupper ($row["str_gen"][$i])]["ok"]++;
        else
          ${"stats_$dl"}[strtoupper ($row["str_gen"][$i])]["nok"]++;
      }

          
  }
  for($dl=1; $dl<=3; $dl++) :
    foreach($alphabet as $char)
        ${"stats_$dl"}[$char]["succes_rate"] = round(100*${"stats_$dl"}[$char]["ok"]/(${"stats_$dl"}[$char]["ok"] + ${"stats_$dl"}[$char]["nok"]));
  endfor;
  ?>
<?php $lettersPerGraph = 11; ?>
  
\usepackage{pgfplots}
\def \letterStatisticsGraph {	
  \begin{figure}[h]
  	\begin{center}
  		\captionsetup[subfigure]{labelformat=empty}
<?php for($graph=0; $graph<3; $graph++) : ?>
    		\subfloat[][]
    		{
    			\begin{tikzpicture}
    			\begin{axis}[
    			width=\textwidth,
    			height=4.5cm,
    			ybar,
    			ymin=0,
    			ymax=100,
    			xmin=<?php echo $alphabet[$graph*$lettersPerGraph]; ?>,
    			xmax=<?php echo $alphabet[($graph+1)*$lettersPerGraph-1]; ?>,
    			x=1.5cm,
    			ybar interval=0.7,
    			legend style={at={(0.5,1.1)},
    				anchor=south,legend columns=-1},
    			ylabel near ticks,
    			ylabel={Success rate},
    			xlabel={Letter},
    			symbolic x coords={ <?php
            for($letterIndex=$graph*$lettersPerGraph; $letterIndex < ($graph+1)*$lettersPerGraph; $letterIndex++)
              echo $alphabet[$letterIndex] . ", "; 
              
          ?>},
    			xtick=data,
    			ymajorgrids=true
    			]
<?php for($plot=1; $plot<4; $plot++) : ?>
    			\addplot coordinates { <?php
            for($letterIndex=$graph*$lettersPerGraph; $letterIndex < ($graph+1)*$lettersPerGraph; $letterIndex++)
              echo "(" . $alphabet[$letterIndex] . "," . ${"stats_$plot"}[$alphabet[$letterIndex]]["succes_rate"] . ") "; 
              
          ?>};
<?php endfor; ?>
    			<?php if($graph==0) : ?>\legend{Variant a,Variant b,Variant c}<?php endif; ?>
    			?>\end{axis}
    			\end{tikzpicture}
    		}\\[-2ex]
<?php endfor; ?>
  		\caption{Success rate of all used characters for all three types of Bubble Captcha}
  		\label{fig:SuccessRate}
  	\end{center}
  \end{figure}
}<?php } /*
print_r($stats_1);
print_r($stats_2);
print_r($stats_3);
*/
?>