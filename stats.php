    <div class="container">
<?php
require_once('sql_config.php');

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT count(*) all_count, (SELECT COUNT(*) FROM captcha WHERE status = 1) sok, COUNT(DISTINCT jmeno) users FROM captcha";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {?>
<h2>Total statistics</h2>
<?php while($row = mysqli_fetch_assoc($result)) : ?>
    <table class="total-stats">
      <tr><th>Unique users:</th><td><?php echo $row["users"]; ?></td></tr>
      <tr><th>Total attemps count:</th><td><?php echo $row["all_count"]; ?></td></tr>
      <tr><th>Succes attemps count:</th><td><?php echo $row["sok"]; ?></td></tr>
      <tr><th>Failed attemps count:</th><td><?php echo $row["all_count"] - $row["sok"]; ?></td></tr>
      <tr><th>Success rate:</th><td><?php echo round(($row["sok"] / $row["all_count"])*100); ?>%</td></tr>
    </table>
<?php endwhile; ?>
    <hr>
<?php } else {
    echo "Bez statistik";
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

$sql = "SELECT diff_lvl, count(*) all_count, (SELECT COUNT(*) FROM captcha a WHERE a.diff_lvl = b.diff_lvl AND a.status = 1) sok FROM captcha b GROUP BY diff_lvl";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {?>

<h2>Total statistics per difficulty level</h2>
    <table>
      <tr>
        <th>Level of difficulty</th>
        <th>Total attemps count</th>
        <th>Succes attemps count</th>
        <th>Failed attemps count</th>
        <th>Success rate</th>
      </tr>
<?php while($row = mysqli_fetch_assoc($result)) : ?>
      <tr>
        <td><?php echo $row["diff_lvl"]; ?></td>
        <td><?php echo $row["all_count"]; ?></td>
        <td><?php echo $row["sok"]; ?></td>
        <td><?php echo $row["all_count"] - $row["sok"]; ?></td>
        <td><?php echo round(($row["sok"] / $row["all_count"])*100); ?>%</td>
      </tr>
<?php endwhile; ?>
    </table>
    <hr>
<?php } else {
    echo "No statistics";
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

$sql = "SELECT jmeno,count(*) all_count, (SELECT count(*) FROM captcha a WHERE a.jmeno = b.jmeno AND a.status = 1) sok FROM captcha b GROUP BY jmeno";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {?>
<h2>Statistics per name</h2>
    <table>
      <tr>
        <th>Entered name</th>
        <th>Total attemps count</th>
        <th>Succes attemps count</th>
        <th>Failed attemps count</th>
        <th>Success rate</th>
      </tr>
<?php while($row = mysqli_fetch_assoc($result)) : ?>
      <tr>
        <td><a href="http://<?php echo $_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"]; ?>?name=<?php echo $row["jmeno"]; ?>"><?php echo $row["jmeno"]; ?></a></td>
        <td><?php echo $row["all_count"]; ?></td>
        <td><?php echo $row["sok"]; ?></td>
        <td><?php echo $row["all_count"] - $row["sok"]; ?></td>
        <td><?php echo round(($row["sok"] / $row["all_count"])*100); ?>%</td>
      </tr>
<?php endwhile; ?>
    </table>
    <h4>To see user details, click on user name.</h4>
<?php } else {
    echo "No statistics";
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
if (isset($_GET['name'])):
  $sql = "SELECT diff_lvl,count(*) all_count, (select count(*) from captcha a where a.diff_lvl = b.diff_lvl and a.status = 1 and jmeno = '" . $_GET['name'] . "') sok from captcha b where jmeno = '" . $_GET['name'] . "' group by diff_lvl";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) > 0){ ?>
  <hr>
  <h2>Statistics for user <?php echo $_GET['name']; ?></h2>
      <table>
        <tr>
          <th>Level of difficulty</th>
          <th>Total attemps count</th>
          <th>Succes attemps count</th>
          <th>Failed attemps count</th>
          <th>Success rate</th>
        </tr>
  <?php while($row = mysqli_fetch_assoc($result)) : ?>
        <tr>
          <td><?php echo $row["diff_lvl"]; ?></td>
          <td><?php echo $row["all_count"]; ?></td>
          <td><?php echo $row["sok"]; ?></td>
          <td><?php echo $row["all_count"] - $row["sok"]; ?></td>
          <td><?php echo round(($row["sok"] / $row["all_count"])*100); ?>%</td>
        </tr>
  <?php endwhile; ?>
      </table>
  <?php } else {
    echo "No statistics";
  }
endif;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
$sql = "SELECT str_gen, str_entered, diff_lvl FROM `captcha`";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0){
  $alphabet = str_split("ABCDEFGHIJKLMNPQRSTUVXYZ123456789");
  $stats = array();
  foreach($alphabet as $char)
  {
    $stats_1[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
    $stats_2[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
    $stats_3[$char] = array( "ok"=>0, "nok"=>0, "succes_rate"=>0, "letter"=>$char);
  }
  while($row = mysqli_fetch_assoc($result))
  {
    if (strlen($row["str_gen"]) == strlen($row["str_entered"]))
      for ($i=0; $i<strlen($row["str_gen"]); $i++) {
        $dl = $row["diff_lvl"];
        if($row["str_gen"][$i] == $row["str_entered"][$i])
          ${"stats_$dl"}[strtoupper ($row["str_gen"][$i])]["ok"]++;
        else
          ${"stats_$dl"}[strtoupper ($row["str_gen"][$i])]["nok"]++;
      }

          
  } ?>
  <hr>
  <h2>Statistics per letter</h2>
  
  <?php for($dl=1; $dl<=3; $dl++) :
    foreach($alphabet as $char)
    {
      ${"stats_$dl"}[$char]["succes_rate"] = round(100*${"stats_$dl"}[$char]["ok"]/(${"stats_$dl"}[$char]["ok"] + ${"stats_$dl"}[$char]["nok"]));
    }
    usort(${"stats_$dl"}, function ($item1, $item2) {
      if ($item1['succes_rate'] == $item2['succes_rate'])
        return $item1['letter'] > $item2['letter'];
      return $item1['succes_rate'] > $item2['succes_rate'];
    });
    ?>
    <div class="del3">
      <h4>Level <?php echo $dl; ?></h4>
      <table style="width: 95%">
        <tr>
          <th>Letter</th>
          <th>Generated</th>
          <th>Succes rate</th>
        </tr>
      <?php foreach(${"stats_$dl"} as $char=>$stat) : ?>
        <tr>
          <td><?php echo $stat["letter"]; ?></td>
          <td><?php echo $stat["ok"] + $stat["nok"]; ?></td>
          <td><?php echo $stat["succes_rate"]; ?>%</td>
        </tr>
      <?php endforeach; ?>
      </table>
    </div>  
<?php  endfor; ?>
<div class="clearfix"></div>
<?php } else {
  echo "No statistics";
}
?>
<a href ="stat_latex.php"><hr /><hr /><hr /></a>
    </div>